let fieldToSortBy = "";
let sortingOrder = "";
const headerLabels = {
    username:"UserName",
    first_name:"FirstName",
    last_name:"LastName",
    gender:"Gender",
}
const baseUrl = "https://services.odata.org/TripPinRESTierService/(S(negfbklwt4uhmeegv0o43zyh))/People";


function onHeaderCellClickHandler (event){
    event.preventDefault();
    const headerLabel = headerLabels[event.target.id]
    if(fieldToSortBy === headerLabel){
        sortingOrder = sortingOrder===""?"asc":sortingOrder==="asc"?"desc":"";
    }else {
        fieldToSortBy = headerLabel;
        sortingOrder = "asc";
    }
    fetchPeople(fieldToSortBy,sortingOrder);
}

async function fetchPeople(field,order){
    document.getElementById("preloader").className="preloader";
    let url = baseUrl;
    let queryString = field?order?`?$orderby=${field}%20${order}`:"":"";
    url+=queryString;
    let people = [];
    if ("fetch" in window) {
        people =  [... await fetchCall(url)];
    } else {
        people = [... await axiosCall(url)];
    }
    updateDOMWithFetchedData(people);
}

async function  fetchCall(url){
    const people = await fetch(url,{
        method:"GET"
    }).then(res=>res.json()).catch(err=>console.log(err));
    if(people) return people.value;
}

async function axiosCall(url){
    const people = await axios(url)
    .then(res=>res.data.value)
    .catch(err=>console.log(err));
    if(people) return people;
}

function updateDOMWithFetchedData(people){
    const dataRows = createTableBody(people);
    document.getElementById("content").innerHTML = dataRows;
    document.getElementById("preloader").className="preloaderHidden"
}

function createTableBody(people){
    let rows ="";
    people.forEach(person =>{
        rows+=createNewDataRow(person)
    })
    return rows;
}

function createNewDataRow(person) {
    const row = `<div class="content__row">
                    <div class="content__item">${person.UserName}</div>
                    <div class="content__item">${person.FirstName}</div>
                    <div class="content__item">${person.LastName}</div>
                    <div class="content__item">${person.Gender}</div>
                </div>`;
    return row;
}

fetchPeople();